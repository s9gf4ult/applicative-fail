# CHANGELOG

## 1.1.1
### Changed
* upper base constraints relaxed

## 1.1.0

### Added

* `mapFailTBase` - map inner monad (e.g. lift)
* `mapFailTFail` - map error type

## 1.0.0

### Added

* Module `Control.Monad.Fail` containing monadic version of
  `Control.Applicative.Fail`
* Tests added: all properties (Monad, Applicative, Monoid laws) are
  prooved with QuickCheck and package `checkers`
* `runFail` and `runDLFail`: functions to unwrap `Fail`
* dependencies added: `dlist`, `mtl`, `transformers`

### Removed

* `fsucc`: use `return` / `pure` instead
* `bindFail` and `composeFail`: use monadic fail instead

### Changed

* `ffail` and `fwarn` renamed to `afail` and `awarn`
* `afail` and `awarn` are more generic now
* documentation strictly improoved

## 0.0.3

First usable version
