# What is it for?

Assume you have some type

```haskell
data Animal = Animal
    { species :: String
    , weight  :: Double
    , age     :: Int
    } deriving (Show)

```

And you would like to produce this value from some data (e.g. query
parameters). There can be some warnigns or value can not be produced
at all. It would be great to have some simple tool to notify about
warnings and/or fail computation.

Like that:

```haskell
let spc = "Parastratiosphecomyia stratiosphecomyioides"
    w = 100
    a = 27234
    animal :: Fail [String] Animal
    animal = Animal
             <$> (if length spc > 20
                  then awarn "Name is too long" spc
                  else if spc == ""
                       then afail "Name can not be empty"
                       else pure spc)
             <*> (if w < 0
                  then afail "Weight can not be negative"
                  else pure w)
             <*> (if a < 0
                  then afail "Age can not be negative"
                  else pure a)
```

Now you can inspect the value we have got

```haskell
>>> animal
Fail ["Name is too long"] (Just (Animal {species = "Parastratiosphecomyia stratiosphecomyioides", weight = 100.0, age = 27234}))

>>> getSucc animal
Just (Animal {species = "Parastratiosphecomyia stratiosphecomyioides", weight = 100.0, age = 27234})

>>> getFail animal
Just ["Name is too long"]
```

Now this example can be rewritten with monadic syntax inside field
checkers using module `Control.Monad.Fail`:

```haskell
let animal :: Fail [String] Animal
    animal = Animal
             <$> (runFailI $ do
                          when (length spc > 20) $ mwarn "Name is too long"
                          when (spc == "") $ mfail "Name can not be empty"
                          return spc)
             <*> (runFailI $ do
                          when (w < 0) $ mfail "Weight can not be negative"
                          return w)
             <*> (runFailI $ do
                          when (a < 0) $ mfail "Age can not be negative"
                          return a)
```
