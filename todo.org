* 1.0.0
** monad part
*** DONE make it transformer
*** DONE add runners transforming back to applicative
*** DONE add runners returning `Compose`
*** DONE add mfail, mwarn like in applicative
*** DONE MonadReader and friends
*** DONE Add Identity convenience wrappers
** applicative part
*** DONE add runners constraining error type
*** DONE make ffail and fwarn more generic
*** DONE remove bindFail and composeFail
** DONE documentation and use cases
** DONE fix readme
** DONE fix changelog
** DONE bump version
* 1.1.0
** DONE mapFailTBase, mapFailTFail
** DONE changelog
** DONE bump version
* 1.2.0
** monad part
*** TODO Conditional compilation
    to be compatible with old and new transformers and
    similar problems
*** TODO MonadBaseControl for FailT
*** TODO MonadCont, MonadFix and such stuff
*** TODO Add newtype wrapper to make Bifunctor instance posible
* sometimes
** TODO early fail
   https://ro-che.info/articles/2015-05-02-smarter-validation
